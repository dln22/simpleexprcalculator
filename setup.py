from setuptools import setup, find_packages

setup(
    name='exprcalc',
    version='0.0.7',
    author='Daniel Nurkowski',
    author_email='daniel@nurkowski.com',
    license='MIT',
    python_requires='>=3.5',
    description="Simple expr calc.",    
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    packages=find_packages(exclude=('tests')),
    #install_requires= [],
    include_package_data= True,
    #entry_points={
    #    'console_scripts': [
    #    ]
    #}
)